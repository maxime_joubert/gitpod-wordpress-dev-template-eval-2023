<?php
//partie 1
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
	$parenthandle = 'twentynineteen-style'; // This is 'twentynineteen-style' for the Twenty Fifteen theme.
	$theme        = wp_get_theme();
	wp_enqueue_style( $parenthandle,
		get_template_directory_uri() . '/style.css',
		array(),  // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
	);
	wp_enqueue_style( 'twentynineteen-child-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' ) // This only works if you have Version defined in the style header.
	);
}

//partie 2
function wporg_recipe_post_type() {
	register_post_type('wporg_product',
		array(
			'labels'      => array(
				'name'          => __('Recipe', 'textdomain'),
				'singular_name' => __('Recipe', 'textdomain'),
			),
				'public'      => true,
				'has_archive' => true,
                'show_in_rest' => true,
                'rewrite'     => array( 'slug' => 'recette' ),
		)
	);
}
add_action('init', 'wporg_custom_post_type');

//partie 3
function wporg_add_infos_complementaires_box() {
	$screens = [ 'post', 'wporg_cpt' ];
	foreach ( $screens as $screen ) {
		add_meta_box(
			'wporg_box_id',                 // Unique ID
			'Infos complémentaires',      // Box title
			'wporg_custom_box_html',  // Content callback, must be of type callable
            array( 'slug' => 'ingredients'),
			$screen
		);
	}
}
add_action( 'add_meta_boxes', 'wporg_add_custom_box' );

?>